<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCoordinatesTable extends Migration
{
    public function up(): void
    {
        Schema::create('coordinates', function (Blueprint $table) {
            $table->id();
            $table->float('latitude', 16, 12);
            $table->float('longitude', 16, 12);
            $table->foreignId('city_id')->constrained('cities');
            $table->unique(['latitude', 'longitude']);
            $table->timestamps();
        });
    }

    public function down(): void
    {
        Schema::dropIfExists('coordinates');
    }
}
