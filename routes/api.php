<?php

use App\Http\Controllers\AddressController;
use App\Http\Controllers\StateController;
use Illuminate\Support\Facades\Route;

Route::get('address', [AddressController::class, 'store']);

Route::resource('states', StateController::class)->only(['index', 'show']);