<?php

namespace App\Rules;

use Illuminate\Contracts\Validation\Rule;

class LatitudeRule implements Rule
{
    private const PATTERN = '/\A[+-]?(?:90(?:\.0{1,18})?|\d(?(?<=9)|\d?)\.\d{1,18})\z/x';

    /**
     * @param  string  $attribute
     * @param  mixed  $value
     * @return bool
     */
    public function passes($attribute, $value): bool
    {
        return preg_match(self::PATTERN, $value);
    }

    public function message(): string
    {
        return 'The :attribute is wrong format.';
    }
}
