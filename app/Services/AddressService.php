<?php

declare(strict_types = 1);

namespace App\Services;

use App\DTO\CoordinateDTO;
use App\Models\State;
use JetBrains\PhpStorm\ArrayShape;

class AddressService
{
    private const CITY_TYPE = 'locality';
    private const STATE_TYPE = 'administrative_area_level_1';

    public function __construct(
        private GeocodingApiService $geocodingApiService,
        private StateService $stateService,
        private CityService $cityService,
        private CoordinateService $coordinateService,
    ) {}

    public function getAddress(CoordinateDTO $coordinateDTO): State
    {
        if ($address = $this->getAddressForCoordinate($coordinateDTO)) {
            return $address;
        }

        $addressFromApi = $this->geocodingApiService->getAddress($coordinateDTO);

        [
            'state' => $state,
            'city' => $city,
        ] = $this->mapAddress($addressFromApi);

        $stateId = $this->stateService->createAndGetId($state);
        $cityId = $this->cityService->createAndGetId($city, $stateId);

        $this->coordinateService->save($coordinateDTO, $cityId);

        return $this->getAddressForCoordinate($coordinateDTO);
    }

    private function getAddressForCoordinate(CoordinateDTO $coordinate): ?State
    {
        if ($this->coordinateService->exists($coordinate)) {
            return $this->stateService->getForCoordinate($coordinate);
        }

        return null;
    }

    #[ArrayShape(['state' => 'string', 'city' => 'string'])]
    private function mapAddress(array $address): array
    {
        $state = '';
        $city = '';

        foreach ($address[0]['address_components'] as $addressComponent) {
            if (in_array(self::CITY_TYPE, $addressComponent['types'], true)) {
                $city = $addressComponent['long_name'];
            } elseif (in_array(self::STATE_TYPE, $addressComponent['types'], true)) {
                $state = $addressComponent['long_name'];
            }
        }

        return [
            'state' => $state,
            'city' => $city,
        ];
    }
}
