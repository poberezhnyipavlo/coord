<?php

declare(strict_types=1);

namespace App\Services;

use App\DTO\CoordinateDTO;
use Exception;
use GuzzleHttp\Client;
use GuzzleHttp\Exception\GuzzleException;
use JetBrains\PhpStorm\Pure;

class GeocodingApiService
{
    private const BASE_URL = 'https://maps.googleapis.com/maps/api/geocode/json';
    private const LAT_LNG = 'latlng';

    public function getAddress(CoordinateDTO $coordinate): array
    {
        return $this->getQuery($this->parseParameters($coordinate, self::LAT_LNG));
    }

    #[Pure]
    private function parseParameters(CoordinateDTO $coordinate, string $getParameterKey): string
    {
        return "{$getParameterKey}={$coordinate->getLatitude()},{$coordinate->getLongitude()}";
    }

    private function getQuery(string $params): ?array
    {
        try {
            $client = new Client();

            try {
                $apiKey = config('geocoding.api_key');

                $response = $client->get(self::BASE_URL . '?' . $params . '&key=' . $apiKey);

                return json_decode(
                    $response->getBody()->getContents(),
                    true,
                    512,
                    JSON_THROW_ON_ERROR,
                )['results'];
            } catch (Exception|GuzzleException $e) {
                return null;
            }
        } catch (Exception $e) {
            return null;
        }
    }
}
