<?php

declare(strict_types=1);

namespace App\Services;

use App\Models\City;

class CityService
{
    public function createAndGetId(string $city, int $stateId): int
    {
        if ($id = $this->exists($city, $stateId)) {
            return $id;
        }

        return $this->create($city, $stateId);
    }

    private function exists(string $city, int $stateId): ?int
    {
        return City::where('name', $city)
            ->where('state_id', $stateId)
            ->first()
            ?->getKey()
        ;
    }

    private function create(string $name, int $stateId): int
    {
        $city = new City();

        $city->name = $name;
        $city->state_id = $stateId;

        $city->save();

        return $city->getKey();
    }
}
