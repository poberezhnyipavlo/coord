<?php

declare(strict_types=1);

namespace App\Services;

use App\DTO\CoordinateDTO;
use App\Models\Coordinate;

class CoordinateService
{
    public function save(CoordinateDTO $coordinateDTO, int $cityId): void
    {
        $model = new Coordinate();

        $model->longitude = $coordinateDTO->getLongitude();
        $model->latitude = $coordinateDTO->getLatitude();
        $model->city_id = $cityId;

        $model->save();
    }

    public function exists(CoordinateDTO $coordinate): bool
    {
        return Coordinate::where('latitude', $coordinate->getLatitude())
            ->where('longitude', $coordinate->getLongitude())
            ->exists()
        ;
    }
}
