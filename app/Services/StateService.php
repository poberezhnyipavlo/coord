<?php

declare(strict_types=1);

namespace App\Services;

use App\DTO\CoordinateDTO;
use App\Models\State;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Support\Collection;

class StateService
{
    public function createAndGetId(string $name): int
    {
        if ($id = $this->exists($name)) {
            return $id;
        }

        return $this->create($name);
    }

    private function exists(string $name): ?int
    {
        return State::where('name', $name)->first()?->getKey();
    }

    private function create(string $name): int
    {
        $model = new State(['name' => $name]);

        $model->save();

        return $model->getKey();
    }

    public function getForCoordinate(CoordinateDTO $coordinate): ?State
    {
        return State::with([
            'cities.coordinates' => fn(HasMany $builder) => $builder->where([
                'longitude' => $coordinate->getLongitude(),
                'latitude' => $coordinate->getLatitude(),
            ]),
        ])->first();
    }

    public function getAll(): Collection
    {
        return State::with(['cities.coordinates'])->get();
    }
}
