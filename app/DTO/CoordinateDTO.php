<?php

declare(strict_types = 1);

namespace App\DTO;

class CoordinateDTO
{
    private float $longitude;
    private float $latitude;

    public function __construct(
        float $longitude,
        float $latitude,
    ) {
        $this->longitude = $longitude;
        $this->latitude = $latitude;
    }

    public function getLongitude(): float
    {
        return $this->longitude;
    }

    public function setLongitude(float $longitude): void
    {
        $this->longitude = $longitude;
    }

    public function getLatitude(): float
    {
        return $this->latitude;
    }

    public function setLatitude(float $latitude): void
    {
        $this->latitude = $latitude;
    }
}
