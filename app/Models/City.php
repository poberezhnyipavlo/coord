<?php

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Support\Collection;

/**
 * Class City
 * @package App\Models
 * @property int id
 * @property string name
 * @property int state_id
 * @property Carbon|string created_at
 * @property Carbon|string updated_at
 * @property Coordinate[]|Collection coordinates
 * @property State state
 */
class City extends Model
{
    use HasFactory;

    protected $fillable = [
        'name',
        'state_id',
    ];

    public function coordinates(): HasMany
    {
        return $this->hasMany(Coordinate::class);
    }

    public function state(): BelongsTo
    {
        return $this->belongsTo(State::class);
    }
}
