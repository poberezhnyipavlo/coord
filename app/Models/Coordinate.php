<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

/**
 * Class Coordinate
 * @package App\Models
 * @property int id
 * @property float latitude
 * @property float longitude
 * @property int city_id
 * @property City city
 */
class Coordinate extends Model
{
    use HasFactory;

    protected $fillable = [
        'latitude',
        'longitude',
        'city_id',
    ];

    public function city(): BelongsTo
    {
        return $this->belongsTo(City::class);
    }
}
