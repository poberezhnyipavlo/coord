<?php

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Support\Collection;

/**
 * Class State
 * @package App\Models
 * @property int id
 * @property string name
 * @property Carbon|string created_at
 * @property Carbon|string updated_at
 * @property City[]|Collection cities
 */
class State extends Model
{
    use HasFactory;

    protected $fillable = [
        'name',
    ];

    public function cities(): HasMany
    {
        return $this->hasMany(City::class);
    }
}
