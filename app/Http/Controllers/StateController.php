<?php

namespace App\Http\Controllers;

use App\Http\Resources\StateResource;
use App\Models\State;
use App\Services\StateService;
use Illuminate\Http\JsonResponse;

class StateController extends Controller
{
    public function __construct(private StateService $stateService) {}

    public function index(): JsonResponse
    {
        $states = $this->stateService->getAll();

        return response()->json(StateResource::collection($states));
    }

    public function show(State $state): JsonResponse
    {
        $state->load('cities.coordinates');

        return response()->json(new StateResource($state));
    }
}
