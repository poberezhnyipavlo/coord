<?php

namespace App\Http\Controllers;

use App\Http\Requests\CoordinateStoreRequest;
use App\Http\Resources\AddressResource;
use App\Services\AddressService;
use Illuminate\Http\JsonResponse;

class AddressController extends Controller
{
    public function __construct(private AddressService $addressService) {}

    public function store(CoordinateStoreRequest $request): JsonResponse
    {
        $address = $this->addressService->getAddress($request->getDTO());

        return response()->json(new AddressResource($address));
    }
}
