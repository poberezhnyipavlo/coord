<?php

namespace App\Http\Requests;

use App\DTO\CoordinateDTO;
use App\Rules\LatitudeRule;
use App\Rules\LongitudeRule;
use Illuminate\Foundation\Http\FormRequest;
use JetBrains\PhpStorm\Pure;

class CoordinateStoreRequest extends FormRequest
{
    public function authorize(): bool
    {
        return true;
    }

    public function rules(): array
    {
        return [
            'latitude' => [
                'required',
                new LatitudeRule(),
            ],
            'longitude'=> [
                'required',
                new LongitudeRule(),
            ],
        ];
    }

    #[Pure]
    public function getDTO(): CoordinateDTO
    {
        return new CoordinateDTO(
            longitude: $this->longitude,
            latitude: $this->latitude,
        );
    }
}
