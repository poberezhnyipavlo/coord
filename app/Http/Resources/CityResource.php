<?php

namespace App\Http\Resources;

use App\Models\City;
use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\JsonResource;

/**
 * Class CityResource
 * @package App\Http\Resources
 * @mixin City
 */
class CityResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param Request $request
     * @return array
     */
    public function toArray($request): array
    {
        return [
            'id' => $this->id,
            'city' => $this->name,
            'coordinates' => CoordinatesResource::collection($this->coordinates),
            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at,
        ];
    }
}
