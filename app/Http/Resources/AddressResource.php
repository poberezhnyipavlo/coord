<?php

namespace App\Http\Resources;

use App\Models\State;
use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\JsonResource;

/**
 * Class AddressResource
 * @package App\Http\Resources
 * @mixin State
 */
class AddressResource extends JsonResource
{
    /**
     * @param Request $request
     * @return array
     */
    public function toArray($request): array
    {
        return [
            'region' => $this->name,
            'city' => $this->cities[0]->name,
            'coordinates' =>  new CoordinatesResource($this->cities[0]->coordinates[0]),
        ];
    }
}
