<?php

namespace App\Http\Resources;

use App\Models\State;
use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\JsonResource;

/**
 * Class StateResource
 * @package App\Http\Resources
 * @mixin State
 */
class StateResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param Request $request
     * @return array
     */
    public function toArray($request): array
    {
        return [
            'id' => $this->id,
            'region' => $this->name,
            'cities' => CityResource::collection($this->cities),
            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at,
        ];
    }
}
